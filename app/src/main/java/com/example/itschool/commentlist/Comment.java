package com.example.itschool.commentlist;

import android.content.Context;

/**
 * Created by IT SCHOOL on 21.12.2016.
 */
public class Comment {
    private String username;
    private String img;
    private String comment;

    public Comment (String username, String comment) {
        this.username = username;
        this.comment = comment;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
