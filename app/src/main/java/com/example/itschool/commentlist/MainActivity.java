package com.example.itschool.commentlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    CommentAdapter adapter;
    Spinner spinner;
    private String[] variants = {"best", "popular", "newest"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        spinner = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter<String> spinneAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                variants);
        spinner.setAdapter(spinneAdapter);

        ArrayList<Comment> list = new ArrayList<>();
        list.add(new Comment("user", "comment1"));
        list.add(new Comment("admin", "comment2"));
        list.add(new Comment("user", "comment1"));
        list.add(new Comment("admin", "comment2"));
        list.add(new Comment("user", "comment1"));
        list.add(new Comment("admin", "comment2"));
        list.add(new Comment("user", "comment1"));
        list.add(new Comment("admin", "comment2"));
        list.add(new Comment("user", "comment1"));
        list.add(new Comment("admin", "comment2"));
        list.add(new Comment("user", "comment1"));
        list.add(new Comment("admin", "comment2"));

        list.add(new Comment("user", "comment1"));
        list.add(new Comment("admin", "comment2"));
        list.add(new Comment("user", "comment1"));
        list.add(new Comment("admin", "comment2"));
        list.add(new Comment("user", "comment1"));
        list.add(new Comment("admin", "comment2"));

        list.add(new Comment("user", "comment1"));
        list.add(new Comment("admin", "comment2"));
        list.add(new Comment("user", "comment1"));
        list.add(new Comment("admin", "comment2"));
        list.add(new Comment("user", "comment1"));
        list.add(new Comment("admin", "comment2"));


        adapter = new CommentAdapter(this, list);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);


    }

}
